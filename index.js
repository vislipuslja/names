/**
 * Created by vladimirkavlakan on 11/11/2016.
 */
const express = require("express");
const app = express();
const cors = require("cors");

app.use(cors());
app.get("/", function(req, res) {
    let result = `Invalid fullname`;
    let fullname = req.query.fullname;
    if (fullname.length == 0) {
        res.send(result);
        return;
    }
    const components = fullname.split(" ").filter(function(el) {
        return el.length > 0 && el.replace(/\s+/g, ``).length > 0
    });

    for (let i = 0; i < components.length; ++i) {
        if (components[i].match(/[0-9-_]    +/g)) {
            console.log(components[i]);
            res.send(result);
            return;
        }
    }

    console.log(components);
    let lastName, firstName, middleName;

    switch (components.length) {
        case 1:
            lastName = components.pop();
            break;
        case 2:
            firstName = components.shift();
            lastName = components.shift();
            break;
        case 3:
            firstName = components.shift();
            middleName = components.shift();
            lastName = components.shift();
            break;
        default:
            res.send(result);
            return;
    }

    lastName = lastName.toLowerCase();
    result = lastName.charAt(0).toUpperCase() + lastName.slice(1);
    if (firstName) {
        result += ` ${firstName[0].toUpperCase()}.`
    }
    if (middleName) {
        result += ` ${middleName[0].toUpperCase()}.`
    }
    res.send(result);
});

app.listen(8080);